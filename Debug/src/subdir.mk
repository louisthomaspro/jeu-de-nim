################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Renderer.cpp \
../src/main.cpp \
../src/nimASCIIRenderer.cpp \
../src/nimConfiguration.cpp \
../src/nimGenerator.cpp \
../src/nimHumain.cpp \
../src/nimIA.cpp \
../src/nimJoueur.cpp \
../src/nimPartie.cpp \
../src/nimRendererAdapter.cpp \
../src/nimSDLRenderer.cpp \
../src/nimStrategie.cpp \
../src/nimStrategieDifficile.cpp \
../src/nimStrategieFacile.cpp \
../src/nimStrategieNormale.cpp 

OBJS += \
./src/Renderer.o \
./src/main.o \
./src/nimASCIIRenderer.o \
./src/nimConfiguration.o \
./src/nimGenerator.o \
./src/nimHumain.o \
./src/nimIA.o \
./src/nimJoueur.o \
./src/nimPartie.o \
./src/nimRendererAdapter.o \
./src/nimSDLRenderer.o \
./src/nimStrategie.o \
./src/nimStrategieDifficile.o \
./src/nimStrategieFacile.o \
./src/nimStrategieNormale.o 

CPP_DEPS += \
./src/Renderer.d \
./src/main.d \
./src/nimASCIIRenderer.d \
./src/nimConfiguration.d \
./src/nimGenerator.d \
./src/nimHumain.d \
./src/nimIA.d \
./src/nimJoueur.d \
./src/nimPartie.d \
./src/nimRendererAdapter.d \
./src/nimSDLRenderer.d \
./src/nimStrategie.d \
./src/nimStrategieDifficile.d \
./src/nimStrategieFacile.d \
./src/nimStrategieNormale.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/louis/eclipse-workspace-cpp/jeu-de-nim/include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


