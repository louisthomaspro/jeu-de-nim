#include "nimRendererAdapter.h"


NimRendererAdapter::NimRendererAdapter(RendererType type) {

    switch(type) {
        case RendererType::SDL:
            _renderer = new NimSDLRenderer();
            break;
        case RendererType::ASCII:
            _renderer = new NimASCIIRenderer();
            break;
    }

}
NimRendererAdapter::~NimRendererAdapter() {
    delete _renderer;
}

void NimRendererAdapter::afficher(const NimConfiguration &config)  const{
    _renderer->draw(config.getNbPierre(LEFT), config.getNbPierre(MIDDLE), config.getNbPierre(RIGHT));
}
