//============================================================================
// Name        : TEST.cpp
// Author      : Louis THOMAS
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "nimPartie.h"

int main()
{

	// l'utilisateur choisit son mode de jeu
	unsigned int mode = 0;
	cout << "Définissez le mode de jeu : " << endl;
	cout << "\t1. Joueur vs IA" << endl;
	cout << "\t2. Joueur vs Joueur" << endl;
	cout << "\t3. IA vs IA" << endl;

	while (cout << "Mode : " && ( !(cin >> mode) || mode < 1 || mode > 3)) {
	    cin.clear(); //clear bad input flag
	    cin.ignore(1000, '\n'); //discard input
	    cout << "Mode incorrect" << endl;;
	}

	// Crée et lance la partie avec le mode
	NimPartie partie((Mode) mode);
	partie.commencer();

    return 0;

}
