#include "nimHumain.h"

NimHumain::NimHumain() {
	cout << "Entrez le pseudo du joueur : ";
	cin >> _pseudo;
}

NimHumain::~NimHumain() {

}

NimTurn NimHumain::jouer(NimConfiguration &config)
{
	cout << "À " << _pseudo << " de jouer !" << endl;

	int colonne = 0;
	cout << "Choisis une colonne (1 à 3). ";


	// si c'est vrai on continue
	while (
			cout << "Colonne : " // vrai
			&& (!(cin >> colonne)
					|| colonne < 1 || colonne > 3 || (config.getNbPierre((Choice) (colonne - 1)) == 0)
			)){
		cin.clear(); //clear bad input flag
		cin.ignore(1000, '\n'); //discard input
		cout << "Choix incorrect" << endl;;
	}

	Choice col = (Choice) (colonne - 1);

	unsigned int nbPierre;
	cout << "Combien de pierre veux-tu retirer ? (1 à " << config.getNbPierre(col) << "). ";
	while (cout << "Nombre de pierre : " && ( !(cin >> nbPierre) || nbPierre < 1 || nbPierre > config.getNbPierre(col))){
		cin.clear(); //clear bad input flag
		cin.ignore(1000, '\n'); //discard input
		cout << "Choix incorrect" << endl;;
	}


	return NimTurn(col, nbPierre);
}
