#include "nimConfiguration.h"

NimConfiguration::NimConfiguration()
{
	NimConfiguration(NimGenerator::rand(10, 20), NimGenerator::rand(10, 20), NimGenerator::rand(10, 20));
}

NimConfiguration::NimConfiguration(unsigned int nbPierreGauche, unsigned int nbPierreMilieu, unsigned int nbPierreDroite)
{
	/*if(nbPierreGauche > NB_MAX_PER_HEAP || nbPierreMilieu > NB_MAX_PER_HEAP || nbPierreDroite > NB_MAX_PER_HEAP)
        throw NimException("Trop de pierre MAX:20");*/
	_quiJoue = false;

	_nbPierre[0]=nbPierreGauche;
	_nbPierre[1]=nbPierreMilieu;
	_nbPierre[2]=nbPierreDroite;
}

bool NimConfiguration::retirerPierre(Choice head, unsigned int nb)
{
	if (_nbPierre[head] < nb)
		return false;
	_nbPierre[head] = _nbPierre[head] - nb;
	return true;
}

bool NimConfiguration::isEmpty()
{
	bool result = true;
	for (unsigned int i = 0; i<3; i++) {
		if (_nbPierre[i] != 0) {
			result = false;
			break;
		}
	}
	return result;
}

unsigned int NimConfiguration::getNbPierre(Choice heap) const
{
	return _nbPierre[heap];
}

bool NimConfiguration::changement() {
	_quiJoue = !_quiJoue;
	return _quiJoue;
}



