#include "nimASCIIRenderer.h"
#include <iostream>

void NimASCIIRenderer::draw(unsigned int l, unsigned int m, unsigned int r) {

    // affiche le nombre de pierres dans chaque paquet
    std::cout << "1: " << l << std::endl
              << "2: " << m << std::endl
              << "3: " << r << std::endl;

}
