#include "nimIA.h"

NimIA::NimIA() : strategie(nullptr) {
	cout << "Entrez le pseudo de l'IA : ";
	cin >> _pseudo;
}

NimIA::~NimIA() {

}

void NimIA::setStrategie(StrategieIA strat)
{
    delete strategie;
    switch(strat)
	{
    case StrategieIA::Facile:
        strategie=new NimStrategieFacile();
        break;
    case StrategieIA::Normal:
        strategie=new NimStrategieNormale();
        break;
    case StrategieIA::Difficile:
        strategie=new NimStrategieDifficile();
	}
}

NimTurn NimIA::jouer(NimConfiguration &config)
{
    if(strategie == nullptr)
        throw NimException("No Strategy has been selected !");

    return strategie->strategie(config);
}
