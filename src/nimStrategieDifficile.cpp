#include "nimStrategieDifficile.h"

NimStrategieDifficile::NimStrategieDifficile()
{

}
NimStrategieDifficile::~NimStrategieDifficile()
{

}

NimTurn NimStrategieDifficile::strategie(NimConfiguration &config)
{
    return expert(config.getNbPierre(LEFT), config.getNbPierre(MIDDLE), config.getNbPierre(RIGHT));
}
