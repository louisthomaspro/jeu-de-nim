#include "nimStrategieFacile.h"

NimStrategieFacile::NimStrategieFacile()
{

}
NimStrategieFacile::~NimStrategieFacile()
{

}

NimTurn NimStrategieFacile::strategie(NimConfiguration &config)
{
    return beginner(config.getNbPierre(LEFT), config.getNbPierre(MIDDLE), config.getNbPierre(RIGHT));
}
