#include "nimStrategieNormale.h"

NimStrategieNormale::NimStrategieNormale()
{

}

NimStrategieNormale::~NimStrategieNormale()
{

}

NimTurn NimStrategieNormale::strategie(NimConfiguration &config)
{
    switch(NimGenerator::rand(0, 1))
    {
    case 0:
         return beginner(config.getNbPierre(LEFT), config.getNbPierre(MIDDLE), config.getNbPierre(RIGHT));
    case 1:
         return expert(config.getNbPierre(LEFT), config.getNbPierre(MIDDLE), config.getNbPierre(RIGHT));
    default: //No warning :)
        return beginner(config.getNbPierre(LEFT), config.getNbPierre(MIDDLE), config.getNbPierre(RIGHT));
    }
}
