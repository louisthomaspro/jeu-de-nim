#include "nimPartie.h"

using namespace std;

NimPartie::NimPartie(Mode mode) : renderers(), config(12, 13, 7){

	switch(mode) {
	case JOUEUR_VS_IA:
		a = new NimHumain();
		b = new NimIA();
		break;
	case JOUEUR_VS_JOUEUR:
		a = new NimHumain();
		b = new NimHumain();
		break;
	case IA_VS_IA:
		a = new NimIA();
		b = new NimIA();
		break;
	}

	renderers.push_back(unique_ptr<Renderer> (new NimRendererAdapter(RendererType::SDL)));
	renderers.push_back(unique_ptr<Renderer> (new NimRendererAdapter(RendererType::ASCII)));
}
NimPartie::~NimPartie()
{
    delete a;
    delete b;
}

void NimPartie::commencer()
{

	NimTurn turn(LEFT,0);

	if(dynamic_cast<NimIA*>(a))
    {
        ((NimIA*)a)->setStrategie(StrategieIA::Facile);
    }
    if(dynamic_cast<NimIA*>(b))
    {
        ((NimIA*)b)->setStrategie(StrategieIA::Normal);
    }


	NimJoueur *player = a;

	while (!config.isEmpty())
	{

		//		SDL_Delay(1000);
		for(auto&& renderer : renderers)
            renderer->afficher(config);

		if (config.changement()) {
			player = a;
		} else {
			player = b;
		}
		turn = player->jouer(config);

		if (dynamic_cast<NimIA*>(player) ) {
			cout << "L'ordinateur " << player->getPseudo() << " retire " << turn.nbStones << " pierres de la pile " << turn.selectedHeap << endl;
		}


		config.retirerPierre(turn.selectedHeap, turn.nbStones);

	}

	//fenetre->draw(config->getNbPierre(LEFT),config->getNbPierre(MIDDLE),config->getNbPierre(RIGHT));

	cout << "FINISHED !!" << endl;
	cout << "Le joueur " << player->getPseudo() << " a gagné !" << endl;
	//	NimSDLRenderer::quit();
}
