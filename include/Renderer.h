#ifndef RENDERER_H
#define RENDERER_H

#include "nimConfiguration.h"


class Renderer
{
public:
    Renderer();
    virtual ~Renderer();

    virtual void afficher(const NimConfiguration &config /* evite de modifier NimConfiguration */) const = 0; // evite de modifer attribut renderer
    
};

#endif
