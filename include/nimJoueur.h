#ifndef NIM_JOUEUR_H
#define NIM_JOUEUR_H


#include "nimTurn.h"
#include "nimConfiguration.h"

#include <string>
#include <iostream>
using namespace std;


class NimJoueur
{
public:
    NimJoueur();
    virtual ~NimJoueur();

    virtual NimTurn jouer(NimConfiguration &config) = 0;

    string getPseudo(); // retourne le pseudo du joueur

protected:
    string _pseudo; // pseudo du joueur

    
};

#endif
