#ifndef NIM_STRATEGIE_H
#define NIM_STRATEGIE_H

#include "nimConfiguration.h"
#include "nimStratIA.h"


class NimStrategie
{
public:
    NimStrategie();
    virtual ~NimStrategie();

    virtual NimTurn strategie(NimConfiguration &config) = 0;



};

#endif
