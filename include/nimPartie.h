#ifndef NIM_PARTIE_H
#define NIM_PARTIE_H

#include <list>
#include <memory>
#include "nimConfiguration.h"
#include "nimIA.h"
#include "nimHumain.h"
#include "Renderer.h"
#include "nimRendererAdapter.h"

//#include "NimRendererAdapter.h"

enum Mode : unsigned int {
	JOUEUR_VS_IA = 1,
    JOUEUR_VS_JOUEUR,
    IA_VS_IA
};

class NimPartie
{

public:

	NimPartie(Mode mode);
	~NimPartie();

	void commencer(); // commence la partie et fait jouer les joueurs



private:
    std::list<unique_ptr<Renderer>> renderers;
    NimConfiguration config;

	NimJoueur *a; // joueur 1
	NimJoueur *b; // joueur 2
};

#endif
