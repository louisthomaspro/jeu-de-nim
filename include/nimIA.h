#ifndef NIM_IA_H
#define NIM_IA_H


#include "nimJoueur.h"
#include "nimException.h"
#include "nimStrategie.h"
#include "nimStrategieFacile.h"
#include "nimStrategieNormale.h"
#include "nimStrategieDifficile.h"

#include <sstream>

enum class StrategieIA {
    Facile,
    Normal,
    Difficile
};

class NimIA : public NimJoueur
{
public:
    NimIA(); // demande le nom joueur
    void setStrategie(StrategieIA strat);
    virtual ~NimIA();

    virtual NimTurn jouer(NimConfiguration &config); // fait jouer le joueur
private:

    NimStrategie *strategie;
};

#endif
