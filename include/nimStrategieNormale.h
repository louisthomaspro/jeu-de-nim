#ifndef NIM_STRATEGIE_NORMALE_H
#define NIM_STRATEGIE_NORMALE_H


#include "nimStrategie.h"



class NimStrategieNormale : public NimStrategie
{
public:
    NimStrategieNormale();
    virtual ~NimStrategieNormale();

    virtual NimTurn strategie(NimConfiguration &config);
    
};

#endif