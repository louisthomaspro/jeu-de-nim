#ifndef NIM_STRATEGIE_DIFFICLE_H
#define NIM_STRATEGIE_DIFFICILE_H

#include <sstream>
#include "nimStrategie.h"



class NimStrategieDifficile : public NimStrategie
{
public:
    NimStrategieDifficile();
    virtual ~NimStrategieDifficile();

    virtual NimTurn strategie(NimConfiguration &config);

};

#endif
