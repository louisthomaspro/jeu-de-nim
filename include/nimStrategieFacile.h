#ifndef NIM_STRATEGIE_FACILE_H
#define NIM_STRATEGIE_FACILE_H


#include "nimStrategie.h"



class NimStrategieFacile : public NimStrategie
{
public:
    NimStrategieFacile();
    virtual ~NimStrategieFacile();

    virtual NimTurn strategie(NimConfiguration &config);

};

#endif
