#ifndef NIM_RENDERER_ADAPTER_H
#define NIM_RENDERER_ADAPTER_H


#include "Renderer.h"
#include "nimASCIIRenderer.h"
#include "nimSDLRenderer.h"
#include "nimRenderer.h"
#include "nimTurn.h"

enum class RendererType
{
    SDL,
    ASCII
};

class NimRendererAdapter : public Renderer
{

public:
    NimRendererAdapter(RendererType type);
    virtual ~NimRendererAdapter();

    virtual void afficher(const NimConfiguration &config) const;

private:
    NimRenderer *_renderer;

};

#endif
