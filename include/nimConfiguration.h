#ifndef NIM_CONFIGURATION_H
#define NIM_CONFIGURATION_H

#include "nimTurn.h"
#include "nimGenerator.h"

#define NB_MAX_PER_HEAP 20

class NimConfiguration
{
public:
	NimConfiguration();
	NimConfiguration(unsigned int nbPierreGauche, unsigned int nbPierreMilieu, unsigned int nbPierreDroite);

	unsigned int getNbPierre(Choice heap) const;
	bool retirerPierre(Choice heap, unsigned int nb); // retire "nb" pierre(s) dans la colonne "heap"
	bool isEmpty(); // renvoi vrai si la configuration ne contient plus de pierre
	bool changement(); // change l'attribue "quiJoue" pour donner la main à l'autre joueur

private:
	unsigned int _nbPierre[3]; // colonnes et pierres
	bool _quiJoue; // quel joueur joue
};

#endif
