#ifndef NIM_HUMAIN_H
#define NIM_HUMAIN_H


#include "nimJoueur.h"


class NimHumain : public NimJoueur
{
public:
    NimHumain(); // demande le nom joueur
    virtual ~NimHumain();

    virtual NimTurn jouer(NimConfiguration &config); // fait jouer le joueur
    
};

#endif
